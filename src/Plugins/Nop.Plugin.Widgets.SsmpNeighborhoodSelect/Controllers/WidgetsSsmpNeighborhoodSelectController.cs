﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Controllers
{
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class WidgetsSsmpNeighborhoodSelectController : BasePluginController
    {
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public WidgetsSsmpNeighborhoodSelectController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        /// <summary>
        /// We get here through routing from  "string SsmpNeighborhoodSelectPlugin:GetConfigurationPageUrl()"
        /// which returns a route - something like "http://dev.sinsingmarketplace.com/Admin/SsmpNeighborhoodSelect/Configure";
        /// </summary>
        /// <returns>the Configure.cshtml view and ConfigurationModel instance</returns>
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var settings = _settingService.LoadSetting<SsmpNeighborhoodSelectSettings>(storeScope);
            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                GooglePlacesApiKey = settings.GooglePlacesApiKey
            };

            if (storeScope > 0)
            {

            }

            return View("~/Plugins/Widgets.SsmpNeighborhoodSelect/Views/Configure.cshtml", model);
        }
    }
}