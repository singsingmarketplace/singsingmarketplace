﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect
{
    /// <summary>
    /// this class holds the values that will go in the settings table
    /// </summary>
    public class SsmpNeighborhoodSelectSettings : ISettings
    {
        public string GooglePlacesApiKey { get; set; }
    }
}