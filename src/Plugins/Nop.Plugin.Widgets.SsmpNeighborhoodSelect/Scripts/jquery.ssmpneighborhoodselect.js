/*
 * jQuery ssmp neighborhoodselect plugin 1.01
 * http://singsingmarketplace.com
 *
 * Copyright 2021, singsing marketplace
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

shopping_place_id_cookie = "shopping_place_id";
shopping_neighborhood_cookie = "shopping_neighborhood";

function initGooglePlaces(countryCodesCommaSep, locationSearchTextbox) {
  // get a handle to the location search text box using its id
  // const locationSearchTextbox = document.getElementById("locationSearch");
  // create an instance of the Google places autocomplete and tie it to the 
  // location search text box
  const locationAutocomplete = new google.maps.places.Autocomplete(locationSearchTextbox);
  // Set country restrictions to the list of countries we have ssmp deployed to
  var listOfCountryCodes = countryCodesCommaSep.split(",");
  locationAutocomplete.setComponentRestrictions({
    country: listOfCountryCodes
  });

  // Avoid paying for data that we don't need by restricting the set of
  // place fields that are returned to just the address components.
  // https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
  locationAutocomplete.setFields(["address_components","place_id"]);

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place. When a user selects a place from the predictions 
  // attached to the autocomplete text field, the service fires a place_changed event.
  //
  // To get place details:
  // 1. Create an event handler for the place_changed event, and call addListener() on 
  //    the Autocomplete object to add the handler.
  //
  // 2. Call Autocomplete.getPlace() on the Autocomplete object, to retrieve a PlaceResult 
  //    object, which you can then use to get more information about the selected place.
  // https://developers.google.com/maps/documentation/javascript/places-autocomplete#get-place-information
  // https://developers.google.com/maps/documentation/javascript/places-autocomplete
  locationAutocomplete.addListener("place_changed", () => {
    const place = locationAutocomplete.getPlace();

    if (place.address_components) {
      const place_id = place.place_id;

      // persist place_id into cookie
      const cookieExpiry = ";expires=Sat, 31 Dec 2050 12:00:00 UTC; path=/";
      document.cookie = shopping_place_id_cookie + "=" + place_id + cookieExpiry;

      // persist location into cookie. the selected shopping location friendly 
      // address is placed in the search location by the google places api
      document.cookie = shopping_neighborhood_cookie + "=" + locationSearchTextbox.value; + cookieExpiry;
    }

  });
}

function deleteNeighborhoodCookie() {
  if (getCookieByName(shopping_neighborhood_cookie)) {
    deleteCookie(shopping_neighborhood_cookie);
  }

  if (getCookieByName(shopping_place_id_cookie)) {
    deleteCookie(shopping_place_id_cookie);
  }
}

function getCookieByName(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function deleteCookie(name) {
  setCookie(name, "", -1);
}

function setCookie(name, value, expirydays) {
  var d = new Date();
  d.setTime(d.getTime() + (expirydays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = name + "=" + value + "; " + expires;
}