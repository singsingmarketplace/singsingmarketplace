﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Models;
using Nop.Services.Configuration;

namespace Nop.Plugin.Widgets.SsmpNeighborhoodSelect.Factories
{
    public class SsmpNeighborhoodSelectModelFactory: ISsmpNeighborhoodSelectModelFactory
    {
        #region constants
        
        /// <summary>
        /// Recently search location
        /// </summary>
        private const string RECENTLY_SEARCH_LOCATION_COOKIE_NAME = ".Nop.RecentlySearchlocation";
        #endregion constants

        private readonly ISettingService _settingService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SsmpNeighborhoodSelectModelFactory(ISettingService settingService,
            IHttpContextAccessor httpContextAccessor)
        {
            _settingService = settingService;
            _httpContextAccessor = httpContextAccessor;
        }

        #region ISsmpNeighborhoodSelectModelFactory implementation
        public virtual PublicInfoModel PreparePublicInfoModel()
        {
            var ssmpNeighborhoodSelectSettings = _settingService.LoadSetting<SsmpNeighborhoodSelectSettings>();
            var model = new PublicInfoModel()
            {
                GooglePlacesApiKey = ssmpNeighborhoodSelectSettings.GooglePlacesApiKey                
            };
            //get recently search location and delivery option from cookie
            model = SetRecentlySearchLocationFromCookie(model);
            return model;
        }
        #endregion ISsmpNeighborhoodSelectModelFactory implementation

        #region Recently search location cookie

        protected virtual PublicInfoModel SetRecentlySearchLocationFromCookie(PublicInfoModel model)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext?.Request == null)
                return model;

            //try to get cookie
            if (!httpContext.Request.Cookies.TryGetValue(RECENTLY_SEARCH_LOCATION_COOKIE_NAME, out string searchlocation) || string.IsNullOrEmpty(searchlocation))
                return model;

            //get array of string product identifiers from cookie
            var lastSearchlocation = searchlocation.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //set placeid
            if (lastSearchlocation.Length > 0)
                model.Pid = lastSearchlocation[0].ToString();
           
            return model;
        }
        
        protected virtual string GetLocationFromCookie()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext?.Request == null)
                return null;

            //try to get cookie
            if (!httpContext.Request.Cookies.TryGetValue(RECENTLY_SEARCH_LOCATION_COOKIE_NAME, out string searchlocation) || string.IsNullOrEmpty(searchlocation))
                return null;
            var recentLocation =  searchlocation.Trim();
            return recentLocation;

        }

        
        /// <summary>
        /// Create Cookies for last User Search location
        /// </summary>
        /// <param name="placeid">placeid</param>
        public virtual void AddRecentlySearchLocationCookie(string placeid)
        {
            if (!string.IsNullOrEmpty(placeid))
            {
                //delete current cookie if exists
                _httpContextAccessor.HttpContext.Response.Cookies.Delete(RECENTLY_SEARCH_LOCATION_COOKIE_NAME);

                //create cookie value
                var locationCookie = placeid;

                //create cookie options 
                var cookieExpires = 24 * 10; //TODO make configurable
                var cookieOptions = new CookieOptions
                {
                    Expires = DateTime.Now.AddHours(cookieExpires),
                    HttpOnly = true
                };

                //add cookie
                _httpContextAccessor.HttpContext.Response.Cookies.Append(RECENTLY_SEARCH_LOCATION_COOKIE_NAME, locationCookie, cookieOptions);
            }
        }

        /// <summary>
        /// get placeid from cookie if any else returen empty string
        /// </summary>
        /// <returns>placeid</returns>
        public virtual string GetPlaceidFromCookie()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext?.Request == null)
                return string.Empty;

            //try to get cookie
            if (!httpContext.Request.Cookies.TryGetValue(RECENTLY_SEARCH_LOCATION_COOKIE_NAME, out string searchlocation) || string.IsNullOrEmpty(searchlocation))
                return null;
            var splitcookievalue = searchlocation.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitcookievalue.Length > 0 && !string.IsNullOrWhiteSpace(splitcookievalue[0]))
                return splitcookievalue[0];
            else
                return string.Empty;

        }
        
        #endregion Recently search location cookie
    }
}
